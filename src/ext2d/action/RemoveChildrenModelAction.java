package ext2d.action;

import ext2d.model.IModel;

public class RemoveChildrenModelAction extends UpdatePropertyAction {

	public static final String ACTION_REMOVE_CHILDREN_MODEL = "Remove children model action";

	public RemoveChildrenModelAction(IModel model) {
		super(ACTION_REMOVE_CHILDREN_MODEL, model);
	}

	@Override
	public IModel getPropertyValue() {
		return (IModel) super.getPropertyValue();
	}

}
