package ext2d.action;

public abstract class AbstractAction implements IAction {

	private String type;

	public AbstractAction(String type) {
		this.type = type;
	}

	@Override
	public String getType() {
		return type;
	}

}
