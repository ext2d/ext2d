package ext2d.action;

public interface IActionListener {

	public void performAction(IAction action);
	
}
