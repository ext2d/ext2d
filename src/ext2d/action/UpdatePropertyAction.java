package ext2d.action;

public class UpdatePropertyAction extends AbstractAction {

	private Object object;

	public UpdatePropertyAction(String type, Object value) {
		super(type);
		this.object = value;
	}

	public Object getPropertyValue() {
		return object;
	}

}
