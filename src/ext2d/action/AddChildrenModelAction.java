package ext2d.action;

import ext2d.model.IModel;

public class AddChildrenModelAction extends UpdatePropertyAction {

	public static final String ACTION_ADD_CHILDREN_MODEL = "Add children model action";

	public AddChildrenModelAction(IModel model) {
		super(ACTION_ADD_CHILDREN_MODEL, model);
	}

	@Override
	public IModel getPropertyValue() {
		return (IModel) super.getPropertyValue();
	}

}
