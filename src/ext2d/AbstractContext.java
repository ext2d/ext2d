package ext2d;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ext2d.controller.IController;
import ext2d.model.IModelWithView;
import ext2d.view.IView;
import ext2d.view.IViewFactory;

public abstract class AbstractContext implements IContext {

	private IViewFactory viewFactory;

	private IController controller;

	private Set<IView> invalidateViews = new HashSet<IView>();

	private Set<IView> validateList = new HashSet<IView>();

	private List<IModelWithView> selection = new ArrayList<IModelWithView>();

	private IModelWithView focused;

	private IModelWithView entered;

	private Map<Object, Image> images = new HashMap<Object, Image>();

	@Override
	public IViewFactory getViewFactory() {
		if (viewFactory == null) {
			viewFactory = createViewFactory();
			viewFactory.setContext(this);
		}
		return viewFactory;
	}

	@Override
	public boolean isDebug() {
		return true;
	}

	public void setController(IController controller) {
		this.controller = controller;
	}

	public void addInvalidView(IView view) {
		invalidateViews.add(view);
	}

	public Set<IView> receiveInvalidViews() {
		validateList.addAll(invalidateViews);
		invalidateViews.clear();
		return validateList;
	}

	abstract protected IViewFactory createViewFactory();

	public void validateAll() {
		controller.validateAll();
	}

	@Override
	public List<IModelWithView> getSelectionList() {
		return selection;
	}

	@Override
	public IModelWithView getFocused() {
		return focused;
	}

	@Override
	public void setFocused(IModelWithView model) {
		focused = model;
	}

	@Override
	public Image getImage(Object object) {
		Image image = images.get(object);
		if (image == null) {
			image = Utils.uploadImage((String) object);
			images.put(object, image);
		}
		return image;
	}

	@Override
	public void repaint() {
		controller.repaint();
	}

	@Override
	public void removeFocus() {
		this.focused = null;
	}

	@Override
	public void mouseEnter(IModelWithView model, MouseEvent e) {
		this.entered = model;
		model.mouseEntered(e);
	}

	@Override
	public void mouseExit(MouseEvent e) {
		if (entered != null) {
			entered.mouseExited(e);
		}
		this.entered = null;
	}

	@Override
	public IModelWithView getMouseEntered() {
		return entered;
	}

	@Override
	public IController getController() {
		return controller;
	}
}
