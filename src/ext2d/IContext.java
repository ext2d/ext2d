package ext2d;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Set;

import ext2d.controller.IController;
import ext2d.model.IModelWithView;
import ext2d.view.IView;
import ext2d.view.IViewFactory;

public interface IContext {

	public IViewFactory getViewFactory();

	public boolean isDebug();

	public void addInvalidView(IView view);

	public Set<IView> receiveInvalidViews();

	public void validateAll();

	public void setController(IController controller);

	public List<IModelWithView> getSelectionList();

	public IModelWithView getFocused();

	public void setFocused(IModelWithView model);

	public Image getImage(Object object);

	public void repaint();

	public void removeFocus();

	public void mouseEnter(IModelWithView model, MouseEvent e);

	public void mouseExit(MouseEvent e);

	public IModelWithView getMouseEntered();

	public IController getController();

}
