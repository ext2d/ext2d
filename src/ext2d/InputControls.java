package ext2d;

public class InputControls {

	public static final int KEY_ENTER = 10;

	public static final int KEY_UP = 38;

	public static final int KEY_DOWN = 40;

	public static final int KEY_ESC = 27;

	public static final int KEY_LEFT = 37;

	public static final int KEY_RIGHT = 39;

}
