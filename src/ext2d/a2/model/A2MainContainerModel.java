package ext2d.a2.model;

import java.awt.event.KeyEvent;

import ext2d.a2.A2Context;
import ext2d.a2.model.menu.MenuModel;
import ext2d.a2.model.playenv.PlayEnvModel;
import ext2d.model.AbstractModelWithView;

public class A2MainContainerModel extends AbstractModelWithView {

	private MenuModel menu;

	private PlayEnvModel playEnv;

	public A2MainContainerModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void refresh() {
		if (menu == null) {
			menu = new MenuModel();
			addModel(menu);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (menu != null && menu.isVisible() && menu.isActive()) {
			menu.keyPressed(e);
		} else if (playEnv != null && playEnv.isVisible() && playEnv.isActive()) {
			playEnv.keyPressed(e);
		}
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	public void startNewGame() {
		if (playEnv != null) {
			if (playEnv.isActive()) {
				playEnv.setActive(false);
				removeModel(playEnv);
				playEnv = null;
			}
		}
		playEnv = new PlayEnvModel();
		addModel(playEnv);
		playEnv.setActive(true);
		playEnv.setVisible(true);
		menu.setVisible(false);
		menu.setActive(false);
	}

	public void pauseGame() {
		menu.setPauseGameMenuMode();
		menu.setActive(true);
		menu.setVisible(true);
		playEnv.setVisible(false);
		playEnv.setActive(false);
	}

	public void continueGame() {
		playEnv.setActive(true);
		playEnv.setVisible(true);
		menu.setVisible(false);
		menu.setActive(false);
	}

	public void stopGame() {
		if (playEnv != null) {
			playEnv.setActive(false);
			removeModel(playEnv);
			playEnv = null;
		}
		menu.setInitialMenuMode();
		menu.setActive(true);
		menu.setVisible(true);
	}

	public void exit() {
		getContext().getController().exit();
	}

	public void continueGameIfItStarted() {
		if (playEnv != null) {
			continueGame();
		}

	}

}
