package ext2d.a2.model.menu;

public class StopGameMenuItemModel extends AbstractMenuItemModel {

	@Override
	public void pressed() {
		getContext().getController().getMainContainer().stopGame();
	}

}
