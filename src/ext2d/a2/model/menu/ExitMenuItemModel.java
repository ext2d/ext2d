package ext2d.a2.model.menu;

public class ExitMenuItemModel extends AbstractMenuItemModel {

	@Override
	public void pressed() {
		getContext().getController().getMainContainer().exit();
	}
}
