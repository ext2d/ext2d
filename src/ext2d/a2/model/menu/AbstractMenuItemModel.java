package ext2d.a2.model.menu;

import java.awt.event.MouseEvent;

import ext2d.a2.A2Context;
import ext2d.model.AbstractModelWithView;

public abstract class AbstractMenuItemModel extends AbstractModelWithView {

	public AbstractMenuItemModel() {
		setMouseSelectable(false);
	}

	@Override
	public void setFocused(boolean isFocused) {
		super.setFocused(isFocused);
		getMenu().setSelected(this, isFocused);
	}

	protected MenuModel getMenu() {
		return (MenuModel) getParentModel();
	}

	abstract public void pressed();

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		pressed();
	}

}
