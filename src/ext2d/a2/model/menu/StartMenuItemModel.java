package ext2d.a2.model.menu;

public class StartMenuItemModel extends AbstractMenuItemModel {

	@Override
	public void pressed() {
		getContext().getController().getMainContainer().startNewGame();
	}

}
