package ext2d.a2.model.menu;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ext2d.InputControls;
import ext2d.a2.A2Context;
import ext2d.model.AbstractModelWithView;

public class MenuModel extends AbstractModelWithView {

	public List<AbstractMenuItemModel> items;

	public MenuModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	public void setPauseGameMenuMode() {
		Iterator<AbstractMenuItemModel> iter = items.iterator();
		while (iter.hasNext()) {
			AbstractMenuItemModel menuItem = iter.next();
			if (menuItem instanceof StartMenuItemModel) {
				menuItem.setActive(false);
				menuItem.setSelected(false);
				menuItem.setVisible(false);
			} else if (menuItem instanceof ContinueGameMenuItemModel) {
				menuItem.setActive(true);
				menuItem.setSelected(true);
				menuItem.setVisible(true);
			} else if (menuItem instanceof StopGameMenuItemModel) {
				menuItem.setActive(true);
				menuItem.setSelected(false);
				menuItem.setVisible(true);
			} else if (menuItem instanceof ExitMenuItemModel) {
				menuItem.setActive(false);
				menuItem.setVisible(false);
				menuItem.setSelected(false);
			}
		}
	}

	public void setInitialMenuMode() {
		Iterator<AbstractMenuItemModel> iter = items.iterator();
		while (iter.hasNext()) {
			AbstractMenuItemModel menuItem = iter.next();
			if (menuItem instanceof StartMenuItemModel) {
				menuItem.setActive(true);
				menuItem.setSelected(true);
				menuItem.setVisible(true);
			} else if (menuItem instanceof ContinueGameMenuItemModel
					|| menuItem instanceof StopGameMenuItemModel) {
				menuItem.setActive(false);
				menuItem.setSelected(false);
				menuItem.setVisible(false);
			} else if (menuItem instanceof ExitMenuItemModel) {
				menuItem.setActive(true);
				menuItem.setVisible(true);
				menuItem.setSelected(false);
			}
		}
	}

	@Override
	public void refresh() {
		if (items == null) {
			items = new ArrayList<AbstractMenuItemModel>();

			StartMenuItemModel startMenuItemModel = new StartMenuItemModel();
			items.add(startMenuItemModel);
			addModel(startMenuItemModel);

			ContinueGameMenuItemModel continueGameMenuItemModel = new ContinueGameMenuItemModel();
			items.add(continueGameMenuItemModel);
			addModel(continueGameMenuItemModel);

			StopGameMenuItemModel stopGameMenuItemModel = new StopGameMenuItemModel();
			items.add(stopGameMenuItemModel);
			addModel(stopGameMenuItemModel);

			ExitMenuItemModel exitMenuItemModel = new ExitMenuItemModel();
			items.add(exitMenuItemModel);
			addModel(exitMenuItemModel);

			setInitialMenuMode();
			
//			// for debug
//			List<AbstractMenuItemModel> items = getVisibleItems();
//			if (items.size() > 1) {
//				Iterator<AbstractMenuItemModel> iter = items.iterator();
//				while (iter.hasNext()) {
//					AbstractMenuItemModel ami = iter.next();
//					if(ami instanceof StartMenuItemModel) {
//						ami.pressed();
//						break;
//					}
//				}
//			}
		
			
		}
		
	}

	protected List<AbstractMenuItemModel> getVisibleItems() {
		Iterator<AbstractMenuItemModel> iter = items.iterator();
		List<AbstractMenuItemModel> visibleItems = new ArrayList<>();
		while (iter.hasNext()) {
			AbstractMenuItemModel menuItem = iter.next();
			if (menuItem.isVisible()) {
				visibleItems.add(menuItem);
			}
		}
		return visibleItems;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == InputControls.KEY_UP
				|| keyCode == InputControls.KEY_DOWN) {
			List<AbstractMenuItemModel> items = getVisibleItems();
			if (items.size() > 1) {
				Iterator<AbstractMenuItemModel> iter = items.iterator();
				AbstractMenuItemModel prevItem = null;
				while (iter.hasNext()) {
					AbstractMenuItemModel menuItem = iter.next();
					if (menuItem.isSelected()) {
						AbstractMenuItemModel nextMenuItem = null;
						if (keyCode == InputControls.KEY_UP) {
							nextMenuItem = prevItem == null ? items.get(items
									.size() - 1) : prevItem;
						} else if (keyCode == InputControls.KEY_DOWN) {
							nextMenuItem = iter.hasNext() ? iter.next() : items
									.get(0);
						}
						menuItem.setSelected(false);
						nextMenuItem.setSelected(true);
						return;
					}
					prevItem = menuItem;
				}
			}
		} else if (keyCode == InputControls.KEY_ENTER) {
			List<AbstractMenuItemModel> items = getVisibleItems();
			Iterator<AbstractMenuItemModel> iter = items.iterator();
			while (iter.hasNext()) {
				AbstractMenuItemModel menuItem = iter.next();
				if (menuItem.isSelected()) {
					menuItem.pressed();
					return;
				}
			}
		} else if (keyCode == InputControls.KEY_ESC) {
			getContext().getController().getMainContainer().continueGameIfItStarted();
		}
	}

	public void setSelected(AbstractMenuItemModel abstractMenuItemModel,
			boolean isSelected) {
		if (isSelected == true) {
			List<AbstractMenuItemModel> items = getVisibleItems();
			Iterator<AbstractMenuItemModel> iter = items.iterator();
			while (iter.hasNext()) {
				AbstractMenuItemModel menuItem = iter.next();
				if (menuItem.isSelected()) {
					if (menuItem == abstractMenuItemModel) {
						return;
					} else {
						menuItem.setSelected(false);
						abstractMenuItemModel.setSelected(true);
						return;
					}
				}
			}
		}
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
