package ext2d.a2.model.menu;

public class ContinueGameMenuItemModel extends AbstractMenuItemModel {

	@Override
	public void pressed() {
		getContext().getController().getMainContainer().continueGame();
	}
}
