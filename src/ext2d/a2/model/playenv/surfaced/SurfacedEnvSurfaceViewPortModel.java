package ext2d.a2.model.playenv.surfaced;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import ext2d.model.AbstractModelWithView;

public class SurfacedEnvSurfaceViewPortModel extends AbstractModelWithView {

	private SurfacedWorldModel surface;

	private int[] xy = new int[2];

	public SurfacedEnvSurfaceViewPortModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void refresh() {
		if (surface == null) {
			surface = new SurfacedWorldModel();
			addModel(surface);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (surface != null && surface.isActive()) {
			surface.keyPressed(e);
		}
	}

	@Override
	public SurfacedEnvInterfaceModel getParentModel() {
		return (SurfacedEnvInterfaceModel) super.getParentModel();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point point = e.getPoint();
		xy[0] = point.x;
		xy[1] = point.y;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point point = e.getPoint();
		int dx = point.x - xy[0];
		int dy = point.y - xy[1];
		surface.move(dx, dy);
		xy[0] = point.x;
		xy[1] = point.y;
	}

}
