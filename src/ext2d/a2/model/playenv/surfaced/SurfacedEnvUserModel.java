package ext2d.a2.model.playenv.surfaced;

import java.awt.event.KeyEvent;

import ext2d.InputControls;
import ext2d.model.AbstractModelWithView;

public class SurfacedEnvUserModel extends AbstractModelWithView {

	private boolean isInitialized = false;

	private int speed = 0;

	private int speedLimit = 20;

	private int speedLimitBack = -5;

	private double angle = 2 * Math.PI / 4;

	public SurfacedEnvUserModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == InputControls.KEY_UP) {
			if (speed < speedLimit)
				speed++;
		} else if (keyCode == InputControls.KEY_DOWN) {
			if (speed > speedLimitBack)
				speed--;
		} else if (keyCode == InputControls.KEY_LEFT) {
			angle -= 0.01;
		} else if (keyCode == InputControls.KEY_RIGHT) {
			angle += 0.01;
		}
	}

	@Override
	public void refresh() {
		if (!isInitialized) {
			isInitialized = true;
		}
	}

	@Override
	public void process() {
		// TODO: Rotate in case of angle
		move(0, -speed);
	}
}
