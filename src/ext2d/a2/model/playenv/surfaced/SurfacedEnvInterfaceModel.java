package ext2d.a2.model.playenv.surfaced;

import java.awt.event.KeyEvent;

import ext2d.model.AbstractModelWithView;

public class SurfacedEnvInterfaceModel extends AbstractModelWithView {

	private SurfacedEnvSurfaceViewPortModel viewPort;

	public SurfacedEnvInterfaceModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void refresh() {
		if (viewPort == null) {
			viewPort = new SurfacedEnvSurfaceViewPortModel();
			addModel(viewPort);
		}
	}

	@Override
	public SurfacedEnvModel getParentModel() {
		return (SurfacedEnvModel) super.getParentModel();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (viewPort != null && viewPort.isActive()) {
			viewPort.keyPressed(e);
		}
	}

}
