package ext2d.a2.model.playenv.surfaced;

import java.awt.event.KeyEvent;

import ext2d.model.AbstractModelWithView;

public class SurfacedEnvModel extends AbstractModelWithView {

	private SurfacedEnvInterfaceModel iface;

	public SurfacedEnvModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void refresh() {
		if (iface == null) {
			iface = new SurfacedEnvInterfaceModel();
			addModel(iface);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (iface != null && iface.isActive()) {
			iface.keyPressed(e);
		}
	}

}
