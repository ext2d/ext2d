package ext2d.a2.model.playenv.surfaced;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import ext2d.model.AbstractModelWithView;

public class SurfacedWorldModel extends AbstractModelWithView {

	private SurfacedEnvUserModel user;

	public SurfacedWorldModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public SurfacedEnvSurfaceViewPortModel getParentModel() {
		return (SurfacedEnvSurfaceViewPortModel) super.getParentModel();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		getParentModel().mousePressed(e);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		getParentModel().mouseDragged(e);
	}

	@Override
	public void refresh() {
		if (user == null) {
			user = new SurfacedEnvUserModel();
			addModel(user);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (user != null && user.isActive()) {
			user.keyPressed(e);
		}
	}

}
