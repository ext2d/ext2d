package ext2d.a2.model.playenv;

import java.awt.event.KeyEvent;

import ext2d.InputControls;
import ext2d.a2.A2Context;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvModel;
import ext2d.model.AbstractModelWithView;

public class PlayEnvModel extends AbstractModelWithView {

	private SurfacedEnvModel surfacedEnvModel;

	public PlayEnvModel() {
		setMouseFocusable(false);
		setMouseSelectable(false);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == InputControls.KEY_ESC) {
			getContext().getController().getMainContainer().pauseGame();
			return;
		}
		if (surfacedEnvModel != null && surfacedEnvModel.isVisible()) {
			surfacedEnvModel.keyPressed(e);
		}
	}

	@Override
	public void refresh() {
		if (surfacedEnvModel == null) {
			surfacedEnvModel = new SurfacedEnvModel();
			addModel(surfacedEnvModel);
		}
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
