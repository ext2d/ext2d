package ext2d.a2;

public class A2Compas {

	public static final int W = 1;

	public static final int WNW = 2;

	public static final int NW = 3;

	public static final int NNW = 4;

	public static final int N = 5;

	public static final int NNE = 6;

	public static final int NE = 7;

	public static final int ENE = 8;

	public static final int E = 9;

	public static final int ESE = 10;

	public static final int SE = 11;

	public static final int SSE = 12;

	public static final int S = 13;

	public static final int SSW = 14;

	public static final int SW = 15;

	public static final int WSW = 16;

	public static int getNextDirection(int direction) {
		assert (direction > W && direction < WSW);
		if (direction == WSW)
			return W;
		return ++direction;
	}

	public static int getPrevDirection(int direction) {
		assert (direction > W && direction < WSW);
		if (direction == W)
			return WSW;
		return --direction;
	}
}
