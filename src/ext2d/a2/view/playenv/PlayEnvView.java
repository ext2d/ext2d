package ext2d.a2.view.playenv;

import ext2d.a2.A2Context;
import ext2d.view.AbstractView;
import ext2d.view.layout.ParentSizeStackedLayoutManager;

public class PlayEnvView extends AbstractView {

	public PlayEnvView() {
		setLayoutManager(new ParentSizeStackedLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
