package ext2d.a2.view.playenv.surfaced;

import java.util.Iterator;

import ext2d.geometry.Rectangle;
import ext2d.view.IView;
import ext2d.view.layout.ILayoutManager;

public class SurfacedEnvViewPortLayoutManager implements ILayoutManager {

	@Override
	public void layout(IView view) {
		Rectangle parentBounds = view.getBounds();
		Iterator<IView> iter = view.getViews().iterator();
		while (iter.hasNext()) {
			IView curView = iter.next();
			if (curView.isVisible()) {
				Rectangle curBounds = curView.getBounds();
				curView.setBounds(curBounds.x, curBounds.y, parentBounds.width,
						parentBounds.height);
			}
		}
	}

	@Override
	public int[] getPreferredSize(IView view) {
		return view.getSize();
	}

}
