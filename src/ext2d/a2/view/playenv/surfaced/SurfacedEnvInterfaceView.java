package ext2d.a2.view.playenv.surfaced;

import ext2d.a2.A2Context;
import ext2d.view.AbstractView;

public class SurfacedEnvInterfaceView extends AbstractView {

	public SurfacedEnvInterfaceView() {
		setLayoutManager(new SurfacedEnvInterfaceLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
