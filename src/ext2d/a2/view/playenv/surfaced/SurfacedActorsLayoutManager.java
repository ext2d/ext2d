package ext2d.a2.view.playenv.surfaced;

import java.util.Iterator;

import ext2d.geometry.Rectangle;
import ext2d.view.IView;
import ext2d.view.layout.ILayoutManager;

public class SurfacedActorsLayoutManager implements ILayoutManager {

	@Override
	public void layout(IView view) {
		Iterator<IView> iter = view.getViews().iterator();
		while (iter.hasNext()) {
			IView curView = iter.next();
			if (curView.isVisible() && curView instanceof SurfacedEnvUserView) {
				int[] prefSize = curView.getPreferredSize();
				Rectangle oldBounds = curView.getBounds();
				curView.setBounds(oldBounds.x, oldBounds.y, prefSize[0],
						prefSize[1]);
			}
		}
	}

	@Override
	public int[] getPreferredSize(IView view) {
		return view.getSize();
	}

}
