package ext2d.a2.view.playenv.surfaced;

import java.awt.Color;
import java.awt.Graphics;

import ext2d.a2.A2Context;
import ext2d.geometry.Rectangle;
import ext2d.view.AbstractView;

public class SurfacedEnvSurfaceViewPortView extends AbstractView {

	public SurfacedEnvSurfaceViewPortView() {
		setLayoutManager(new SurfacedEnvViewPortLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	@Override
	public void paintView(Graphics g) {
		Rectangle bounds = getBounds();
		g.setColor(Color.BLACK);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

}
