package ext2d.a2.view.playenv.surfaced;

import ext2d.a2.A2Context;
import ext2d.view.AbstractView;
import ext2d.view.layout.ParentSizeStackedLayoutManager;

public class SurfacedEnvView extends AbstractView {

	public SurfacedEnvView() {
		setLayoutManager(new ParentSizeStackedLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}
}
