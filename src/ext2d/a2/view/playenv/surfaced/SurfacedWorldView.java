package ext2d.a2.view.playenv.surfaced;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;

import ext2d.a2.A2Context;
import ext2d.a2.model.playenv.surfaced.SurfacedWorldModel;
import ext2d.geometry.Rectangle;
import ext2d.view.AbstractView;

public class SurfacedWorldView extends AbstractView {

	private Image surfaceImage;

	public SurfacedWorldView() {
		setLayoutManager(new SurfacedActorsLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	@Override
	public SurfacedWorldModel getModel() {
		return (SurfacedWorldModel) super.getModel();
	}

	@Override
	public void paintView(Graphics g) {
		Rectangle bounds = getBounds();
		Shape shape = g.getClip();
		g.clipRect(0, 0, bounds.width, bounds.height);
		g.drawImage(getSurfaceImage(), bounds.x, bounds.y, null);
		g.setClip(shape);
	}

	public Image getSurfaceImage() {
		if (surfaceImage == null) {
			surfaceImage = getContext().getImage("surface1.jpg");
		}
		return surfaceImage;
	}

}
