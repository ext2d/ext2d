package ext2d.a2.view.playenv.surfaced;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;

import ext2d.a2.A2Context;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvUserModel;
import ext2d.geometry.Rectangle;
import ext2d.view.AbstractView;

public class SurfacedEnvUserView extends AbstractView {

	private Image userImage;

	public SurfacedEnvUserView() {
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	public Image getUserImage() {
		if (userImage == null) {
			userImage = getContext().getImage("user.png");
		}
		return userImage;
	}

	@Override
	public SurfacedEnvUserModel getModel() {
		return (SurfacedEnvUserModel) super.getModel();
	}

	@Override
	public int[] getPreferredSize() {
		return new int[] { getUserImage().getWidth(null),
				getUserImage().getHeight(null) };
	}

	@Override
	public void paintView(Graphics g) {
		Rectangle bounds = getBounds();
		Shape shape = g.getClip();
		g.clipRect(bounds.x, bounds.y, bounds.width, bounds.height);
		g.drawImage(getUserImage(), bounds.x, bounds.y, null);
		g.setClip(shape);
	}

}
