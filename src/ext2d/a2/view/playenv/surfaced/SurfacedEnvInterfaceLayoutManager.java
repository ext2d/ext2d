package ext2d.a2.view.playenv.surfaced;

import java.util.Iterator;

import ext2d.geometry.Rectangle;
import ext2d.view.IView;
import ext2d.view.layout.ILayoutManager;

public class SurfacedEnvInterfaceLayoutManager implements ILayoutManager {

	private int top = 10;

	private int bottom = 100;

	private int left = 10;

	private int right = 10;

	@Override
	public void layout(IView view) {
		Rectangle parentBounds = view.getBounds();
		Iterator<IView> iter = view.getViews().iterator();
		while (iter.hasNext()) {
			IView curView = iter.next();
			if (curView instanceof SurfacedEnvSurfaceViewPortView) {
				if (curView.isVisible()) {
					curView.setBounds(parentBounds.x + left, parentBounds.y
							+ top, parentBounds.width - left - right,
							parentBounds.height - top - bottom);
				}
			}
		}
	}

	@Override
	public int[] getPreferredSize(IView view) {
		return view.getSize();
	}

}
