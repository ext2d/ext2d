package ext2d.a2.view.menu;

import ext2d.a2.A2Context;

public class StopGameMenuItemView extends AbstractMenuItemView {

	public StopGameMenuItemView() {
		super("Stop game");
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
