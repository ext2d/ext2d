package ext2d.a2.view.menu;

import java.awt.Color;
import java.awt.Graphics;

import ext2d.a2.A2Context;
import ext2d.geometry.Rectangle;
import ext2d.view.AbstractView;

public class MenuView extends AbstractView {

	public MenuView() {
		setLayoutManager(new MenuLayoutManager());
	}

	@Override
	public void paintView(Graphics g) {
		Rectangle bounds = getBounds();
		g.setColor(isSelected() ? Color.DARK_GRAY : Color.BLACK);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
