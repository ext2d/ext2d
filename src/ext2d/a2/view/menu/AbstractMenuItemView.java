package ext2d.a2.view.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import ext2d.a2.A2Context;
import ext2d.geometry.Rectangle;
import ext2d.view.AbstractView;

public class AbstractMenuItemView extends AbstractView {

	private String text;

	private Font font = new Font("Serif", Font.BOLD, 24);

	private Color activeColor = new Color(120, 130, 120);

	private Color inactiveColor = new Color(70, 70, 70);

	public AbstractMenuItemView(String text) {
		this.text = text;
	}

	@Override
	public int[] getPreferredSize() {
		return new int[] { 300, 40 };
	}

	@Override
	public void paintView(Graphics g) {
		Rectangle bounds = getBounds();
		g.setColor(isSelected() ? activeColor : inactiveColor);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
		g.setFont(font);
		g.setColor(Color.BLACK);
		g.drawString(text,
				bounds.x + bounds.width / 2 - text.length() * 18 / 2, bounds.y
						+ bounds.height / 2 + 10);
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
