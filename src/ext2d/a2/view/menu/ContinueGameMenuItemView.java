package ext2d.a2.view.menu;

import ext2d.a2.A2Context;

public class ContinueGameMenuItemView extends AbstractMenuItemView {

	public ContinueGameMenuItemView() {
		super("Continue game");
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
