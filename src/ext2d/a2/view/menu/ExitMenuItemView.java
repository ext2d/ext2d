package ext2d.a2.view.menu;

import ext2d.a2.A2Context;

public class ExitMenuItemView extends AbstractMenuItemView {

	public ExitMenuItemView() {
		super("Exit");
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
