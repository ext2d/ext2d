package ext2d.a2.view.menu;

import java.util.Iterator;
import java.util.List;

import ext2d.geometry.Rectangle;
import ext2d.view.IView;
import ext2d.view.layout.ILayoutManager;

public class MenuLayoutManager implements ILayoutManager {

	protected int space = 10;

	@Override
	public void layout(IView view) {
		Rectangle bounds = view.getBounds();
		List<IView> views = view.getViews();
		if (views.size() == 0)
			return;

		int maxWidth = 0;
		int maxHeight = 0;
		int counter = 0;
		Iterator<IView> iterSizes = views.iterator();
		while (iterSizes.hasNext()) {
			IView curView = iterSizes.next();
			if (curView instanceof AbstractMenuItemView && curView.isVisible()) {
				int[] prefSize = curView.getPreferredSize();
				if (prefSize[0] > maxWidth)
					maxWidth = prefSize[0];
				if (prefSize[1] > maxHeight)
					maxHeight = prefSize[1];
				counter++;
			}
		}

		int curX = bounds.x + bounds.width / 2 - maxWidth / 2;
		int curY = bounds.y + bounds.height / 2
				- ((maxHeight * counter + space * (counter - 1)) / 2);

		Iterator<IView> iter = views.iterator();
		while (iter.hasNext()) {
			IView curView = iter.next();
			if (curView instanceof AbstractMenuItemView & curView.isVisible()) {
				curView.setBounds(curX, curY, maxWidth, maxHeight);
				curY += maxHeight + space;
			}
		}
		return;
	}

	@Override
	public int[] getPreferredSize(IView view) {
		return view.getSize();
	}

}
