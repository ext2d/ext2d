package ext2d.a2.view;

import ext2d.a2.model.A2MainContainerModel;
import ext2d.a2.model.menu.AbstractMenuItemModel;
import ext2d.a2.model.menu.ContinueGameMenuItemModel;
import ext2d.a2.model.menu.ExitMenuItemModel;
import ext2d.a2.model.menu.MenuModel;
import ext2d.a2.model.menu.StartMenuItemModel;
import ext2d.a2.model.menu.StopGameMenuItemModel;
import ext2d.a2.model.playenv.PlayEnvModel;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvInterfaceModel;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvModel;
import ext2d.a2.model.playenv.surfaced.SurfacedWorldModel;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvSurfaceViewPortModel;
import ext2d.a2.model.playenv.surfaced.SurfacedEnvUserModel;
import ext2d.a2.view.menu.AbstractMenuItemView;
import ext2d.a2.view.menu.ContinueGameMenuItemView;
import ext2d.a2.view.menu.ExitMenuItemView;
import ext2d.a2.view.menu.MenuView;
import ext2d.a2.view.menu.NewGameMenuItemView;
import ext2d.a2.view.menu.StopGameMenuItemView;
import ext2d.a2.view.playenv.PlayEnvView;
import ext2d.a2.view.playenv.surfaced.SurfacedEnvInterfaceView;
import ext2d.a2.view.playenv.surfaced.SurfacedWorldView;
import ext2d.a2.view.playenv.surfaced.SurfacedEnvSurfaceViewPortView;
import ext2d.a2.view.playenv.surfaced.SurfacedEnvUserView;
import ext2d.a2.view.playenv.surfaced.SurfacedEnvView;
import ext2d.model.IModelWithView;
import ext2d.view.AbstractViewFactory;
import ext2d.view.IView;

public class A2ViewFactory extends AbstractViewFactory {

	@Override
	public IView createView(IModelWithView model) {
		if (model instanceof A2MainContainerModel)
			return new A2MainContainerView();
		if (model instanceof MenuModel)
			return new MenuView();
		if (model instanceof AbstractMenuItemModel)
			return createMenuItemView(model);
		if (model instanceof PlayEnvModel)
			return new PlayEnvView();
		if (model instanceof SurfacedEnvModel)
			return new SurfacedEnvView();
		if (model instanceof SurfacedWorldModel)
			return new SurfacedWorldView();
		if (model instanceof SurfacedEnvUserModel)
			return new SurfacedEnvUserView();
		if (model instanceof SurfacedEnvInterfaceModel)
			return new SurfacedEnvInterfaceView();
		if (model instanceof SurfacedEnvSurfaceViewPortModel)
			return new SurfacedEnvSurfaceViewPortView();
		return null;
	}

	public AbstractMenuItemView createMenuItemView(IModelWithView model) {
		if (model instanceof StartMenuItemModel)
			return new NewGameMenuItemView();
		if (model instanceof ExitMenuItemModel)
			return new ExitMenuItemView();
		if (model instanceof ContinueGameMenuItemModel)
			return new ContinueGameMenuItemView();
		if (model instanceof StopGameMenuItemModel)
			return new StopGameMenuItemView();
		return null;
	}

}
