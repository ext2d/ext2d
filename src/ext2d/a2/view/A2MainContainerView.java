package ext2d.a2.view;

import ext2d.a2.A2Context;
import ext2d.view.AbstractView;
import ext2d.view.layout.ParentSizeStackedLayoutManager;

public class A2MainContainerView extends AbstractView {

	public A2MainContainerView() {
		setLayoutManager(new ParentSizeStackedLayoutManager());
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

}
