package ext2d.a2;

import ext2d.base.BaseSystem;
import ext2d.controller.IController;

public class A2BaseSystem extends BaseSystem {

	private static final long serialVersionUID = 1L;

	@Override
	protected IController createController(Object object) {
		return new A2Controller(object, this);
	}

}
