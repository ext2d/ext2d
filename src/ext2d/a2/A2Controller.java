package ext2d.a2;

import java.awt.Component;
import java.awt.event.KeyEvent;

import ext2d.IContext;
import ext2d.a2.model.A2MainContainerModel;
import ext2d.controller.AbstractController;
import ext2d.model.IModel;
import ext2d.view.layout.ParentSizeStackedLayoutManager;

public class A2Controller extends AbstractController {

	private A2MainContainerModel container;

	private Object repaintSystem;

	private A2BaseSystem baseSystem;

	private Thread process;

	public A2Controller(Object object, A2BaseSystem a2BaseSystem) {
		setLayoutManager(new ParentSizeStackedLayoutManager());
		repaintSystem = object;
		this.baseSystem = a2BaseSystem;
		process = new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					tick();
				}
			}
		};
		process.start();
	}

	@Override
	public void invalidate() {
		super.invalidate();
		baseSystem.repaint();
	}

	@Override
	protected IContext createContext() {
		return new A2Context();
	}

	@Override
	public void refresh() {
		if (container == null) {
			container = new A2MainContainerModel();
			addModel(container);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (container != null) {
			container.keyPressed(e);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (container != null) {
			container.keyReleased(e);
		}
	}

	@Override
	public void repaint() {
		((Component) repaintSystem).repaint();
	}

	@Override
	public A2Context getContext() {
		return (A2Context) super.getContext();
	}

	public A2MainContainerModel getMainContainer() {
		return container;
	}

	public void exit() {
		stopThreads();
		baseSystem.exit();
	}

	@Override
	public void tick() {
		for (IModel model : getModels()) {
			model.tick();
		}
		process();
	}

	@Override
	public void process() {

	}
}
