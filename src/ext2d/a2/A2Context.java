package ext2d.a2;

import ext2d.AbstractContext;
import ext2d.a2.view.A2ViewFactory;
import ext2d.view.IViewFactory;

public class A2Context extends AbstractContext {

	@Override
	protected IViewFactory createViewFactory() {
		return new A2ViewFactory();
	}

	@Override
	public A2Controller getController() {
		return (A2Controller) super.getController();
	}

}
