package ext2d.sandbox;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.JFrame;

public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Random random = new Random(new Random().nextLong());

	public Test() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1500, 1000);
		setVisible(true);
	}

	private static float[][] generateWhiteNoise(int width, int height) {
		float[][] noise = new float[width][height];

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				noise[i][j] = (float) random.nextDouble() % 1;
			}
		}

		return noise;
	}

	private static float[][] generateSmoothNoise(float[][] baseNoise, int octave) {
		int width = baseNoise.length;
		int height = baseNoise[0].length;

		float[][] smoothNoise = new float[width][height];

		int samplePeriod = 1 << octave; // calculates 2 ^ k
		float sampleFrequency = 1.0f / samplePeriod;

		for (int i = 0; i < width; i++) {
			// calculate the horizontal sampling indices
			int sample_i0 = (i / samplePeriod) * samplePeriod;
			int sample_i1 = (sample_i0 + samplePeriod) % width; // wrap around
			float horizontal_blend = (i - sample_i0) * sampleFrequency;

			for (int j = 0; j < height; j++) {
				// calculate the vertical sampling indices
				int sample_j0 = (j / samplePeriod) * samplePeriod;
				int sample_j1 = (sample_j0 + samplePeriod) % height; // wrap
																		// around
				float vertical_blend = (j - sample_j0) * sampleFrequency;

				// blend the top two corners
				float top = interpolate(baseNoise[sample_i0][sample_j0],
						baseNoise[sample_i1][sample_j0], horizontal_blend);

				// blend the bottom two corners
				float bottom = interpolate(baseNoise[sample_i0][sample_j1],
						baseNoise[sample_i1][sample_j1], horizontal_blend);

				// final blend
				smoothNoise[i][j] = interpolate(top, bottom, vertical_blend);
			}
		}

		return smoothNoise;
	}

	private static float interpolate(float x0, float x1, float alpha) {
		return x0 * (1 - alpha) + alpha * x1;
	}

	private static float[][] generatePerlinNoise(float[][] baseNoise,
			int octaveCount) {
		int width = baseNoise.length;
		int height = baseNoise[0].length;

		float[][][] smoothNoise = new float[octaveCount][][]; // an array of 2D
																// arrays
																// containing

		float persistance = 0.5f;

		// generate smooth noise
		for (int i = 0; i < octaveCount; i++) {
			smoothNoise[i] = generateSmoothNoise(baseNoise, i);
		}

		float[][] perlinNoise = new float[width][height];
		float amplitude = 10.1f; // the bigger, the more big mountains
		float totalAmplitude = 0.01f;

		// blend noise together
		for (int octave = octaveCount - 1; octave >= 0; octave--) {
			amplitude *= persistance;
			totalAmplitude += amplitude;

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					perlinNoise[i][j] += smoothNoise[octave][i][j] * amplitude;
				}
			}
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				perlinNoise[i][j] /= totalAmplitude;
				perlinNoise[i][j] = (float) (Math.floor(perlinNoise[i][j] * 25));
			}
		}

		return perlinNoise;
	}

	float[][] noise;

	public void paint(Graphics graphics) {
		Rectangle bounds = getBounds();
		if (noise == null) {
			noise = generatePerlinNoise(
					generateWhiteNoise(bounds.width, bounds.height), 5/*
																	 * octave
																	 * count
																	 */);
		}

		int max = (int) noise[0][0];
		int min = (int) noise[0][0];
		for (int i = 0; i < bounds.width; i++) {
			for (int j = 0; j < bounds.height; j++) {
				if (max < noise[i][j]) {
					max = (int) noise[i][j];
				}
				if (min > noise[i][j]) {
					min = (int) noise[i][j];
				}
			}
		}

		int sr = 255-160;
		int sg = 228-160;
		int sb = 181-160;

		/*int gr = 100;
		int gg = 150;
		int gb = 100;*/

		
		int startLimit = 5;
		
		
		for (int i = 0; i < bounds.width; i++) {
			for (int j = 0; j < bounds.height; j++) {
				int dec = (int)noise[i][j] - startLimit;
				
				Color color = null;
				//if(dec>10) {
					int r = fix(sr - 1*dec);
					int g = fix((int)(sg + 1.5*dec));
					int b = fix(sb -1*dec);
					color = new Color(r,g,b);
					color = color.darker();
					//color = color.darker();
				//} else {
////					int r = fix(sr + dec);
////					int g = fix(sg + dec);
////					int b = fix(sb + dec);
//					color = new Color(r,g,b);
				//}
		

				
				graphics.setColor(color);
				graphics.drawRect(bounds.x + i, bounds.y + j, 1, 1);
			}
		}
	}
	
	public int fix(int value) {
		if(value > 255)
			return 255;
		if(value < 0)
			return 0;
		return value;
	}

	public static void main(String[] args) {
		new Test();
	}

}
