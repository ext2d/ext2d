package ext2d.geometry;

public class Rectangle {

	public int x;

	public int y;

	public int width;

	public int height;

	public Rectangle() {
		this(0, 0, 0, 0);
	}

	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Rectangle) {
			Rectangle bounds = (Rectangle) object;
			return bounds.x == x && bounds.y == y && bounds.width == width
					&& bounds.height == height;
		}
		return false;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + width + ", " + height + ")";
	}
}
