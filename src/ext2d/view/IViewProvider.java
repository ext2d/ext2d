package ext2d.view;

public interface IViewProvider {

	public IView getView();

}
