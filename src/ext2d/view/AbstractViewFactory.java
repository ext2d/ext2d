package ext2d.view;

import ext2d.IContext;
import ext2d.model.IModelWithView;

public abstract class AbstractViewFactory implements IViewFactory {

	private IContext context;

	@Override
	public IContext getContext() {
		return context;
	}

	@Override
	public void setContext(IContext context) {
		this.context = context;
	}

	@Override
	public IView createInitializedView(IModelWithView model) {
		IView view = createView(model);
		view.setModel(model);
		view.setContext(context);
		return view;
	}

	public abstract IView createView(IModelWithView model);
}
