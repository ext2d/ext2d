package ext2d.view;

import ext2d.IContextProvider;
import ext2d.model.IModelWithView;

public interface IViewFactory extends IContextProvider {

	public IView createInitializedView(IModelWithView model);

}
