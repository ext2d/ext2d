package ext2d.view;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ext2d.IContext;
import ext2d.action.AddChildrenModelAction;
import ext2d.action.IAction;
import ext2d.action.RemoveChildrenModelAction;
import ext2d.action.UpdatePropertyAction;
import ext2d.base.Properties;
import ext2d.geometry.Rectangle;
import ext2d.model.IModel;
import ext2d.model.IModelWithView;
import ext2d.view.layout.ILayoutManager;

public class AbstractView implements IView {

	private IModelWithView model;

	private IView parent;

	protected IContext context;

	private List<IView> views = new ArrayList<IView>();

	private boolean isValid = true;

	private ILayoutManager manager;

	private Map<String, Object> properties;

	public AbstractView() {
		properties = Properties.getDefaultProperties();
	}

	@Override
	public IModel getModel() {
		return model;
	}

	@Override
	public void setBounds(Rectangle bounds) {
		setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	@Override
	public Rectangle getBounds() {
		return (Rectangle) properties.get(Properties.PROP_BOUNDS);
	}

	@Override
	public void paint(Graphics g) {
		if (isVisible()) {
			paintView(g);
			if (!views.isEmpty()) {
				Rectangle bounds = getBounds();
				g.translate(bounds.x, bounds.y);
				Iterator<IView> iter = views.iterator();
				while (iter.hasNext()) {
					IView view = iter.next();
					view.paint(g);
				}
				g.translate(-bounds.x, -bounds.y);
			}
		}
	}

	@Override
	public void setModel(IModel model) {
		this.model = (IModelWithView) model;
	}

	@Override
	public IContext getContext() {
		return context;
	}

	@Override
	public void setContext(IContext context) {
		this.context = context;
	}

	@Override
	public List<IView> getViews() {
		return views;
	}

	@Override
	public boolean addView(IView view) {
		if (!views.contains(view)) {
			if (views.add(view)) {
				view.setParent(this);
				invalidate();
				view.invalidate();
			}
		}
		return false;
	}

	@Override
	public boolean removeView(IView view) {
		if (views.remove(view)) {
			view.setParent(null);
			invalidate();
		}
		return false;
	}

	protected void paintView(Graphics g) {

	}

	@Override
	public void performAction(IAction action) {
		if (action instanceof UpdatePropertyAction
				&& properties.containsKey(action.getType())) {
			Object value = ((UpdatePropertyAction) action).getPropertyValue();
			if (action.getType().equals(Properties.PROP_SELECTED)) {
				setSelected((boolean) value);
			} else if (action.getType().equals(Properties.PROP_FOCUSED)) {
				setFocused((boolean) value);
			} else if (action.getType().equals(Properties.PROP_BOUNDS)) {
				setBounds((Rectangle) value);
			} else {
				setProperty(action.getType(),
						((UpdatePropertyAction) action).getPropertyValue());
			}
		} else if (action instanceof AddChildrenModelAction) {
			IModel model = ((AddChildrenModelAction) action).getPropertyValue();
			if (model instanceof IModelWithView) {
				addView(((IModelWithView) model).getView());
			}
		} else if (action instanceof RemoveChildrenModelAction) {
			IModel model = ((AddChildrenModelAction) action).getPropertyValue();
			if (model instanceof IModelWithView) {
				removeView(((IModelWithView) model).getView());
			}
		}
	}

	@Override
	public IView getView(int x, int y) {
		if (isVisible()) {
			Rectangle bounds = getBounds();
			if (x >= bounds.x && x < bounds.x + bounds.width && y >= bounds.y
					&& y < bounds.y + bounds.height) {
				for (int i = views.size() - 1; i >= 0; i--) {
					IView curView = views.get(i);
					if (curView == null || !curView.isVisible())
						continue;
					IView view = curView.getView(x - bounds.x, y - bounds.y);
					if (view != null)
						return view;
				}
				return this;
			}
		}
		return null;
	}

	@Override
	public boolean isValid() {
		return isValid;
	}

	@Override
	public void invalidate() {
		if (isValid) {
			isValid = false;
			getContext().addInvalidView(this);
			if (getParent() != null) {
				getParent().invalidate();
			}
		}
	}

	@Override
	public void invalidateTree() {
		invalidate();
		Iterator<IView> iter = views.iterator();
		while (iter.hasNext()) {
			iter.next().invalidateTree();
		}
	}

	@Override
	public void validate() {
		if (!isValid) {
			validateView();
			isValid = true;
			Iterator<IView> iter = views.iterator();
			while (iter.hasNext()) {
				iter.next().validate();
			}
		}
	}

	public void validateView() {
		if (manager == null) {
		} else {
			manager.layout(this);
		}
	}

	@Override
	public ILayoutManager getLayoutManager() {
		return manager;
	}

	@Override
	public int[] getPreferredSize() {
		if (manager == null) {
			return getSize();
		}
		return manager.getPreferredSize(this);
	}

	@Override
	public int[] getSize() {
		Rectangle bounds = getBounds();
		return new int[] { bounds.width, bounds.height };
	}

	public void setLayoutManager(ILayoutManager manager) {
		this.manager = manager;
	}

	@Override
	public IView getParent() {
		return parent;
	}

	@Override
	public void setParent(IView view) {
		this.parent = view;
	}

	@Override
	public boolean isSelected() {
		return (boolean) properties.get(Properties.PROP_SELECTED);
	}

	@Override
	public boolean isVisible() {
		return (boolean) properties.get(Properties.PROP_VISIBLE);
	}

	@Override
	public boolean isFocused() {
		return (boolean) properties.get(Properties.PROP_FOCUSED);
	}

	@Override
	public boolean isActive() {
		return (boolean) properties.get(Properties.PROP_ACTIVE);
	}

	@Override
	public void setProperty(String name, Object value) {
		Object currentValue = properties.get(name);
		if (!value.equals(currentValue)) {
			properties.put(name, value);
			getModel().performAction(new UpdatePropertyAction(name, value));
			invalidate();
		}
	}

	@Override
	public void setActive(boolean isActive) {
		setProperty(Properties.PROP_ACTIVE, isActive);
	}

	@Override
	public void setVisible(boolean isVisible) {
		setProperty(Properties.PROP_VISIBLE, isVisible);
	}

	@Override
	public void setSelected(boolean isSelected) {
		setProperty(Properties.PROP_SELECTED, isSelected);
	}

	@Override
	public void setFocused(boolean isFocused) {
		setProperty(Properties.PROP_FOCUSED, isFocused);
	}

	@Override
	public void setBounds(int x, int y, int width, int height) {
		Rectangle bounds = getBounds();
		if (bounds.x != x || bounds.y != y || bounds.width != width
				|| bounds.height != height) {
			bounds.x = x;
			bounds.y = y;
			bounds.width = width;
			bounds.height = height;
			if (getModel() != null) {
				getModel()
						.performAction(
								new UpdatePropertyAction(
										Properties.PROP_BOUNDS, bounds));
			}
			invalidate();
		}
	}

	@Override
	public void disposeView() {
		if (isSelected()) {
			setSelected(false);
		}
		if (isFocused()) {
			setFocused(false);
		}
		if (isActive()) {
			setActive(false);
		}
	}

}
