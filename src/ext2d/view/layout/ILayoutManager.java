package ext2d.view.layout;

import ext2d.view.IView;

public interface ILayoutManager {

	public void layout(IView view);

	public int[] getPreferredSize(IView view);

}
