package ext2d.view.layout;

import java.util.Iterator;

import ext2d.geometry.Rectangle;
import ext2d.view.IView;

public class ParentSizeStackedLayoutManager implements ILayoutManager {

	@Override
	public void layout(IView view) {
		Rectangle parentBounds = view.getBounds();
		Iterator<IView> iter = view.getViews().iterator();
		while (iter.hasNext()) {
			IView curView = iter.next();
			if (curView.isVisible()) {
				curView.setBounds(0, 0, parentBounds.width, parentBounds.height);
			}
		}
	}

	@Override
	public int[] getPreferredSize(IView view) {
		return view.getSize();
	}
}
