package ext2d.view;

import java.awt.Graphics;
import java.util.List;

import ext2d.IContextProvider;
import ext2d.action.IActionListener;
import ext2d.geometry.Rectangle;
import ext2d.model.IModelProvider;
import ext2d.view.layout.ILayoutManager;

public interface IView extends IModelProvider, IContextProvider,
		IActionListener {

	public void setBounds(Rectangle boudns);

	public void setBounds(int x, int y, int width, int height);

	public Rectangle getBounds();

	public void paint(Graphics g);

	public List<IView> getViews();

	public boolean addView(IView view);

	public boolean removeView(IView view);

	public IView getView(int x, int y);

	public void validate();

	public void invalidate();

	public void invalidateTree();

	public boolean isValid();

	public IView getParent();

	public void setParent(IView parent);

	public void setLayoutManager(ILayoutManager manager);

	public boolean isSelected();

	public boolean isFocused();

	public boolean isVisible();

	public void setVisible(boolean isVisible);

	public int[] getPreferredSize();

	public int[] getSize();

	public void setSelected(boolean isSelected);

	public void setFocused(boolean isFocused);

	public ILayoutManager getLayoutManager();

	public void disposeView();

	public boolean isActive();

	public void setActive(boolean isActive);
	
	public void setProperty(String name, Object value);

}
