package ext2d.base;

import java.util.HashMap;
import java.util.Map;

import ext2d.geometry.Rectangle;

public class Properties {

	public static final String PROP_VISIBLE = "PROP_VISIBLE";
	public static final String PROP_SELECTED = "PROP_SELECTED";
	public static final String PROP_FOCUSED = "PROP_FOCUSED";
	public static final String PROP_ACTIVE = "PROP_ACTIVE";
	public static final String PROP_BOUNDS = "PROP_BOUNDS";

	public static Map<String, Object> getDefaultProperties() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(PROP_VISIBLE, true);
		props.put(PROP_SELECTED, false);
		props.put(PROP_FOCUSED, false);
		props.put(PROP_ACTIVE, true);
		props.put(PROP_BOUNDS, new Rectangle());
		return props;
	}

}
