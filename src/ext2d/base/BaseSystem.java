package ext2d.base;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;

import ext2d.controller.IController;
import ext2d.geometry.Rectangle;

public abstract class BaseSystem extends JFrame {

	private static final long serialVersionUID = 1L;

	protected IController controller;

	private int bufferWidth;
	private int bufferHeight;
	private Image bufferImage;
	private Graphics bufferGraphics;

	public BaseSystem() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new LayoutManager() {

			@Override
			public void removeLayoutComponent(Component comp) {
			}

			@Override
			public Dimension preferredLayoutSize(Container parent) {
				return null;
			}

			@Override
			public Dimension minimumLayoutSize(Container parent) {
				return null;
			}

			@Override
			public void layoutContainer(Container parent) {
				java.awt.Rectangle bounds = parent.getBounds();
				getController().setBounds(
						new Rectangle(bounds.x, bounds.y + 39, bounds.width,
								bounds.height));
				getController().getContext().validateAll();
			}

			@Override
			public void addLayoutComponent(String name, Component comp) {
			}
		});

		addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				getController().mouseReleased(e);
				getController().getContext().validateAll();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				getController().mousePressed(e);
				getController().getContext().validateAll();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				getController().mouseExited(e);
				getController().getContext().validateAll();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				getController().mouseEntered(e);
				getController().getContext().validateAll();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				getController().mouseClicked(e);
				getController().getContext().validateAll();
			}
		});

		addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				getController().mouseWheelMoved(e);
				getController().getContext().validateAll();
			}
		});

		addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				getController().mouseMoved(e);
				getController().getContext().validateAll();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				getController().mouseDragged(e);
				getController().getContext().validateAll();
			}
		});
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				getController().keyTyped(e);
				getController().getContext().validateAll();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				getController().keyReleased(e);
				getController().getContext().validateAll();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				getController().keyPressed(e);
				getController().getContext().validateAll();
			}
		});

		setSize(640, 480);
		setVisible(true);
	}

	abstract protected IController createController(Object object);

	protected IController getController() {
		if (controller == null) {
			controller = createController(this);
			controller.invalidate();
			controller.refresh();
			controller.startThreads();
		}
		return controller;
	}

	public void paintBuffer(Graphics g) {
		if (g instanceof Graphics2D) {
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_STROKE_CONTROL,
					RenderingHints.VALUE_STROKE_NORMALIZE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_ALPHA_INTERPOLATION,
					RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_ENABLE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		}
		getController().validate();
		getController().paint(g);
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

	private void resetBuffer() {
		bufferWidth = getSize().width;
		bufferHeight = getSize().height;

		if (bufferGraphics != null) {
			bufferGraphics.dispose();
			bufferGraphics = null;
		}
		if (bufferImage != null) {
			bufferImage.flush();
			bufferImage = null;
		}
		System.gc();

		bufferImage = createImage(bufferWidth, bufferHeight);
		bufferGraphics = bufferImage.getGraphics();
	}

	public void paint(Graphics g) {
		if (bufferWidth != getSize().width || bufferHeight != getSize().height
				|| bufferImage == null || bufferGraphics == null)
			resetBuffer();

		if (bufferGraphics != null) {
			bufferGraphics.clearRect(0, 0, bufferWidth, bufferHeight);

			paintBuffer(bufferGraphics);

			g.drawImage(bufferImage, 0, 0, this);
		}
	}

	public void exit() {
		dispose();
	}

}
