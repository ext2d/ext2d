package ext2d;

public interface IContextProvider {

	public IContext getContext();

	public void setContext(IContext context);

}
