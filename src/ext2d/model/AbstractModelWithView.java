package ext2d.model;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import ext2d.action.AddChildrenModelAction;
import ext2d.action.IAction;
import ext2d.action.RemoveChildrenModelAction;
import ext2d.action.UpdatePropertyAction;
import ext2d.base.Properties;
import ext2d.geometry.Rectangle;
import ext2d.view.IView;

public abstract class AbstractModelWithView extends AbstractModel implements
		IModelWithView {

	private IView view;

	private boolean isSelectable = true;

	private boolean isFocusable = true;

	public AbstractModelWithView() {
		properties = Properties.getDefaultProperties();
	}

	@Override
	public IView getView() {
		if (view == null) {
			view = getContext().getViewFactory().createInitializedView(this);
			initView(view);
		}
		return view;
	}

	@Override
	public boolean isMouseSelectable() {
		return isSelectable;
	}

	@Override
	public void setMouseSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}

	protected Rectangle getBounds() {
		return (Rectangle) properties.get(Properties.PROP_BOUNDS);
	}

	protected void initView(IView view) {
	}

	public void move(int dx, int dy) {
		if (dx != 0 || dy != 0) {
			Rectangle bounds = getBounds();
			bounds.x += dx;
			bounds.y += dy;
			notifyUpdateBounds();
		}
	}

	public void setLocation(int x, int y) {
		Rectangle bounds = getBounds();
		if (bounds.x != x || bounds.y != y) {
			bounds.x = x;
			bounds.y = y;
			notifyUpdateBounds();
		}
	}

	public void setBounds(Rectangle bounds) {
		Rectangle curBounds = getBounds();
		if (!curBounds.equals(bounds)) {
			curBounds.x = bounds.x;
			curBounds.y = bounds.y;
			curBounds.width = bounds.width;
			curBounds.height = bounds.height;
			notifyUpdateBounds();
		}
	}

	private void notifyUpdateBounds() {
		if (getView() != null) {
			getView().performAction(
					new UpdatePropertyAction(Properties.PROP_BOUNDS,
							getBounds()));
		}
	}

	@Override
	public void performAction(IAction action) {
		if (action instanceof UpdatePropertyAction
				&& properties.containsKey(action.getType())) {
			Object value = ((UpdatePropertyAction) action).getPropertyValue();
			if (action.getType().equals(Properties.PROP_SELECTED)) {
				setSelected((boolean) value);
			} else if (action.getType().equals(Properties.PROP_FOCUSED)) {
				setFocused((boolean) value);
			} else if (action.getType().equals(Properties.PROP_BOUNDS)) {
				setBounds((Rectangle) value);
			} else {
				setProperty(action.getType(),
						((UpdatePropertyAction) action).getPropertyValue());
			}
		}
	}

	@Override
	public boolean addModel(IModel model) {
		if (super.addModel(model)) {
			getView().performAction(new AddChildrenModelAction(model));
			model.refresh();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeModel(IModel model) {
		if (super.removeModel(model)) {
			model.disposeModel();
			performAction(new RemoveChildrenModelAction(model));
			return true;
		}
		return false;
	}

	@Override
	public void refresh() {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public boolean isVisible() {
		return (boolean) properties.get(Properties.PROP_VISIBLE);
	}

	@Override
	public boolean isSelected() {
		return (boolean) properties.get(Properties.PROP_SELECTED);
	}

	@Override
	public boolean isFocused() {
		return (boolean) properties.get(Properties.PROP_FOCUSED);
	}

	@Override
	public void setProperty(String name, Object value) {
		Object curValue = properties.get(name);
		if (!curValue.equals(value)) {
			properties.put(name, value);
			getView().performAction(new UpdatePropertyAction(name, value));
		}
	}

	@Override
	public void setVisible(boolean isVisible) {
		setProperty(Properties.PROP_VISIBLE, isVisible);
	}

	@Override
	public void setActive(boolean isActive) {
		setProperty(Properties.PROP_ACTIVE, isActive);
	}

	@Override
	public void setSelected(boolean isSelected) {
		boolean oldIsSelected = isSelected();
		setProperty(Properties.PROP_SELECTED, isSelected);
		if (isSelected == true && oldIsSelected != isSelected) {
			getContext().getSelectionList().add(this);
		}
	}

	@Override
	public void setFocused(boolean isFocused) {
		boolean oldIsFocused = isFocused();
		setProperty(Properties.PROP_FOCUSED, isFocused);
		if (oldIsFocused != isFocused) {
			if (isFocused == true) {
				if (isFocused == true) {
					getContext().setFocused(this);
				} else {
					getContext().removeFocus();
				}
			}
		}
	}

	@Override
	public boolean isMouseFocusable() {
		return isFocusable;
	}

	@Override
	public void setMouseFocusable(boolean isFocusable) {
		this.isFocusable = isFocusable;
	}

	@Override
	public void disposeModel() {
		if (isSelected())
			setSelected(false);
		if (isFocused())
			setFocused(false);
		super.disposeModel();
	}
}
