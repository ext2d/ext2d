package ext2d.model;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import ext2d.view.IViewProvider;

public interface IModelWithView extends IModel, IViewProvider {

	public void mouseReleased(MouseEvent e);

	public void mousePressed(MouseEvent e);

	public void mouseExited(MouseEvent e);

	public void mouseEntered(MouseEvent e);

	public void mouseClicked(MouseEvent e);

	public void mouseWheelMoved(MouseWheelEvent e);

	public void mouseMoved(MouseEvent e);

	public void mouseDragged(MouseEvent e);

	public void keyTyped(KeyEvent e);

	public void keyReleased(KeyEvent e);

	public void keyPressed(KeyEvent e);

	public boolean isVisible();

	public void setVisible(boolean isVisible);

	public boolean isMouseSelectable();

	public void setMouseSelectable(boolean isSelectable);

	public boolean isSelected();

	public void setSelected(boolean isSelected);

	public boolean isMouseFocusable();

	public void setMouseFocusable(boolean isFocusable);

	public boolean isFocused();

	public void setFocused(boolean isFocused);

}
