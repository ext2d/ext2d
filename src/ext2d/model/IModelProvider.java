package ext2d.model;

public interface IModelProvider {

	public IModel getModel();

	public void setModel(IModel model);

}
