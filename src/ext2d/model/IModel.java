package ext2d.model;

import java.util.Collection;

import ext2d.IContextProvider;
import ext2d.action.IActionListener;

public interface IModel extends IContextProvider, IActionListener {

	public boolean addModel(IModel model);

	public boolean removeModel(IModel model);

	public Collection<IModel> getModels();

	public void refresh();

	public IModel getParentModel();

	public void setParentModel(IModel model);

	public void disposeModel();

	public boolean isActive();

	public void setActive(boolean isActive);

	public void setProperty(String name, Object value);

	public void tick();

	public void process();

}
