package ext2d.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import ext2d.IContext;
import ext2d.action.IAction;
import ext2d.base.Properties;

public abstract class AbstractModel implements IModel {

	private IContext context;

	private IModel parentModel;

	private HashSet<IModel> models = new HashSet<IModel>();

	protected Map<String, Object> properties;

	@Override
	public IContext getContext() {
		return context;
	}

	@Override
	public void setContext(IContext context) {
		this.context = context;
	}

	@Override
	public void performAction(IAction action) {
	}

	@Override
	public Collection<IModel> getModels() {
		return models;
	}

	@Override
	public boolean addModel(IModel model) {
		model.setContext(getContext());
		boolean isAdded = models.add(model);
		if (isAdded) {
			model.setParentModel(this);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeModel(IModel model) {
		boolean isRemoved = models.remove(model);
		if (isRemoved) {
			model.setParentModel(null);
			return true;
		}
		return false;
	}

	public void setParentModel(IModel model) {
		this.parentModel = model;
	}

	@Override
	public IModel getParentModel() {
		return parentModel;
	}

	@Override
	public void disposeModel() {
	}

	@Override
	public boolean isActive() {
		return (boolean) properties.get(Properties.PROP_ACTIVE);
	}

	@Override
	public void setActive(boolean isActive) {
		if (!properties.equals(isActive))
			properties.put(Properties.PROP_ACTIVE, isActive);
	}

	@Override
	public void tick() {
		for (IModel model : getModels()) {
			model.tick();
		}
		process();
	}

	@Override
	public void process() {

	}

}
