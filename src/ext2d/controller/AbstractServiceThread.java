package ext2d.controller;

import ext2d.IContext;
import ext2d.IContextProvider;

public abstract class AbstractServiceThread extends Thread implements
		IContextProvider {

	private boolean stopFlag = false;

	private int speed;

	private IContext context;

	public AbstractServiceThread(IContext context) {
		this(context, 10);
	}

	public AbstractServiceThread(IContext context, int speed) {
		this.speed = speed;
	}

	@Override
	public void run() {
		while (true) {
			try {
				sleep(speed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (stopFlag) {
				stopFlag = false;
				break;
			}
		}
	}

	abstract public void workTime();

	public void sendStop() {
		this.stopFlag = true;
	}

	@Override
	public IContext getContext() {
		return context;
	}

	@Override
	public void setContext(IContext context) {
		this.context = context;
	}

}
