package ext2d.controller;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import ext2d.model.IModel;
import ext2d.view.IView;
import ext2d.view.IViewProvider;

public interface IController extends IView, IViewProvider, IModel {

	public void mouseReleased(MouseEvent e);

	public void mousePressed(MouseEvent e);

	public void mouseExited(MouseEvent e);

	public void mouseEntered(MouseEvent e);

	public void mouseClicked(MouseEvent e);

	public void mouseWheelMoved(MouseWheelEvent e);

	public void mouseMoved(MouseEvent e);

	public void mouseDragged(MouseEvent e);

	public void keyTyped(KeyEvent e);

	public void keyReleased(KeyEvent e);

	public void keyPressed(KeyEvent e);

	public void refresh();
	
	public void validateAll();

	public void repaint();
	
	public void startThreads();
	
	public void stopThreads();
	
}
