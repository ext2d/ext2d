package ext2d.controller;

import ext2d.IContext;

public class ServiceThread extends AbstractServiceThread {

	public ServiceThread(IContext context) {
		super(context);
	}

	@Override
	public void workTime() {
		getContext().repaint();
	}

}
