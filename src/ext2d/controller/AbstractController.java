package ext2d.controller;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ext2d.IContext;
import ext2d.action.AddChildrenModelAction;
import ext2d.action.RemoveChildrenModelAction;
import ext2d.action.UpdatePropertyAction;
import ext2d.base.Properties;
import ext2d.model.IModel;
import ext2d.model.IModelWithView;
import ext2d.view.AbstractView;
import ext2d.view.IView;

public abstract class AbstractController extends AbstractView implements
		IController {

	abstract protected IContext createContext();

	private HashSet<IModel> models = new HashSet<IModel>();

	private ServiceThread serviceThread;

	@Override
	public IContext getContext() {
		if (context == null) {
			context = createContext();
			context.setController(this);
		}
		return context;
	}

	@Override
	public IView getView() {
		return this;
	}

	@Override
	public IModel getModel() {
		return this;
	}

	@Override
	public void refresh() {

	}

	public List<IModelWithView> getSelection() {
		return getContext().getSelectionList();
	}

	public void mouseReleased(MouseEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel model = view.getModel();
			if (model instanceof IModelWithView) {
				((IModelWithView) model).mouseReleased(e);
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel model = view.getModel();
			if (model instanceof IModelWithView) {
				((IModelWithView) model).mousePressed(e);
			}
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		getContext().mouseExit(e);
		getContext().removeFocus();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		mouseMoved(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel imodel = view.getModel();
			if (imodel instanceof IModelWithView) {
				IModelWithView model = (IModelWithView) imodel;
				if (!getContext().getSelectionList().contains(this)) {
					UpdatePropertyAction removeSelectionAction = new UpdatePropertyAction(
							Properties.PROP_SELECTED, false);
					Iterator<IModelWithView> iter = getContext()
							.getSelectionList().iterator();
					while (iter.hasNext()) {
						IModelWithView curModel = iter.next();
						if (curModel.isMouseSelectable()) {
							curModel.performAction(removeSelectionAction);
							iter.remove();
						}
					}
					if (model.isMouseSelectable()) {
						model.performAction(new UpdatePropertyAction(
								Properties.PROP_SELECTED, true));
					}
				}
				model.mouseClicked(e);
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel model = view.getModel();
			if (model instanceof IModelWithView) {
				((IModelWithView) model).mouseWheelMoved(e);
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel model = view.getModel();
			if (model instanceof IModelWithView) {
				IModelWithView modelWithView = (IModelWithView) model;

				IModelWithView curEnteredModel = getContext().getMouseEntered();
				if (curEnteredModel != modelWithView) {
					getContext().mouseExit(e);
				}
				getContext().mouseEnter(modelWithView, e);

				IModelWithView currentFocused = getContext().getFocused();
				if (currentFocused != null) {
					if (currentFocused != modelWithView) {
						currentFocused.setFocused(false);
					}
				}
				if (modelWithView.isMouseFocusable()) {
					modelWithView.setFocused(true);
				}
				((IModelWithView) model).mouseMoved(e);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		java.awt.Point point = e.getPoint();
		IView view = getView(point.x, point.y);
		if (view != null) {
			IModel model = view.getModel();
			if (model instanceof IModelWithView) {
				((IModelWithView) model).mouseDragged(e);
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public Collection<IModel> getModels() {
		return models;
	}

	@Override
	public boolean addModel(IModel model) {
		model.setContext(getContext());
		if (models.add(model)) {
			model.setParentModel(this);
			performAction(new AddChildrenModelAction(model));
			model.refresh();
			validateAll();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeModel(IModel model) {
		if (models.remove(model)) {
			performAction(new RemoveChildrenModelAction(model));
			model.setParentModel(null);
			validateAll();
			return true;
		}
		return false;
	}

	public void validateAll() {
		Set<IView> invViews = getContext().receiveInvalidViews();
		Iterator<IView> iter = invViews.iterator();
		while (iter.hasNext()) {
			iter.next().validate();
			iter.remove();
		}
		repaint();
	}

	@Override
	public IModel getParentModel() {
		return null;
	}

	@Override
	public void setParentModel(IModel model) {
	}

	@Override
	public void startThreads() {
		if (serviceThread == null) {
			serviceThread = new ServiceThread(getContext());
		}
		serviceThread.start();
	}

	@Override
	public void stopThreads() {
		if (serviceThread != null) {
			serviceThread.sendStop();
		}
	}

	@Override
	public void disposeModel() {
		if (isSelected())
			setSelected(false);
		if (isFocused())
			setFocused(false);
	}

}
