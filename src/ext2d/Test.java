package ext2d;

public class Test {

	private Thread process = new Thread() {
		public void run() {
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				tick();
			}
		}
	};

	public static void main(String[] args) {
		Test test = new Test();
		test.process.start();
	}

	private void tick() {
		System.out.println("tick");
	}

}
